﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public GameObject spinPrefab;
    private Transform startPoint;
    private Transform spawnPoint;
    private Spin currSpin;

    private bool isGameOver = false;
    // Use this for initialization
    void Start () {
        startPoint = GameObject.Find("StartPoint").transform;
        spawnPoint = GameObject.Find("SpawnPoint").transform;
        spawnSpin();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isGameOver)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            currSpin.StartFly();
            spawnSpin();
        }
    }

    void spawnSpin(){
        //GetComponent -- >获取GameObject的组件，包括脚本
        currSpin = GameObject.Instantiate(spinPrefab, spawnPoint.position, spinPrefab.transform.rotation).GetComponent<Spin>();
    }

    public void GameOver()
    {
        if (isGameOver) return;

        GameObject.Find("Circle").GetComponent<RotateSelf>().enabled = false;

        isGameOver = true;
    }
}
