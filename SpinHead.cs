﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinHead : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "SpinHead")
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().GameOver();
        }
    }
}
