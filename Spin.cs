﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour {
    private float speed     = 15;
    private bool isFly      = false;
    private bool isReach    = false;
    private Transform targetPos;
    private Transform circle;
    private Vector3 targetCirclePos;
	// Use this for initialization
	void Start () {
        targetPos   = GameObject.Find("StartPoint").transform;
        circle      = GameObject.Find("Circle").transform;
        targetCirclePos = circle.position;
        targetCirclePos.y = targetCirclePos.y - 1;
    }
	
	// Update is called once per frame
	void Update () {
        if (isFly == false){
            if (isReach == false){
                transform.position = Vector3.MoveTowards(transform.position, targetPos.position, speed * Time.deltaTime);
                if (Vector3.Distance(transform.position, targetPos.position) < 0.05f)
                {
                    isReach = true;
                }
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, targetCirclePos, speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, targetCirclePos) < 0.05f)
            {
                transform.position = targetCirclePos;
                transform.parent = circle;
                isFly = false;
            }
        }
    }

    public void StartFly()
    {
        isFly = true;
        isReach = true;
    }
}
